import axios from 'axios'
import { xml2json, xml2js } from 'xml-js'
import parser from 'fast-xml-parser'

export default (fastify, opts, next) => {

  fastify.get('/', async (request, reply) => {
    const response = await axios.get('https://www.chronopost.fr/recherchebt-ws-cxf/PointRelaisServiceWS/rechercheBtParCodeproduitEtCodepostalEtDate?codePostal=28500&date=27/07/2010')

    reply.send(parser.parse(response.data))
  })

  next()
}

// optional
export const autoPrefix = '/'
